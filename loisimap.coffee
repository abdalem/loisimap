class @LoisiMap
    constructor: (items, marker_icons) ->
        @categories = []
        @items = items
        @marker_icons = marker_icons
        @bounds = new google.maps.LatLngBounds()
        @geocoder = new google.maps.Geocoder()
        @initialize_search_input()
        @initialize_map()
        @initialize_markers_cluster()
        

    initialize_map: => 
        @map = new google.maps.Map(document.getElementById("maps-container"))
        @filtered_items = @items
        @set_markers()
        @fit_to_france()

        @map.setOptions({styles: [
          {
            featureType: 'poi',
            stylers: [{visibility: 'off'}]
          }
        ]})

        google.maps.event.addListener @map, 'click', ->
            @infobox.close() if @infobox

    fit_to_france: ->
        @city_marker.setMap(null) if @city_marker
        country = "France"
        @geocoder.geocode {address: country}, (results) =>
            @map.fitBounds(results[0].geometry.bounds)
            @map.setZoom(6)

    filter_items_by_categories: () =>
        if Array.isArray(@categories) && @categories.length > 0 
            @filtered_items = _.filter @items, (item, index) =>
                if @categories.indexOf(item.t) == -1 then false else true
        else
            @filtered_items = @items
            
    set_categories: (categories) ->
        @categories = categories
        @reset_markers(@items)

    open_info_window: (markers) =>
        ibOptions =
            disableAutoPan: false
            maxWidth: 0
            pixelOffset: new google.maps.Size(-140, 0)
            zIndex: null
            boxStyle:
                padding: "0px 0px 0px 0px"
                width: "270px"
            closeBoxURL : "boutique_fichiers/close.svg"
            infoBoxClearance: new google.maps.Size(1, 1)
            isHidden: false
            pane: "floatPane"
            enableEventPropagation: false

        @infobox.close() if @infobox
        navbar = "<ul class='navbar'>"
        body = "<div class='triangle-with-shadow'></div><div class='body'>"
        for i in [0...markers.length]
            navbar += "<li><a href='#tabs-" + i + "'></a></li>"
            body += "<div id='tabs-" + i + "'><a href='#'>
                <div class='image'><div class='item-bckg' style='background-image: url(boutique_fichiers/sn6.jpg);'></div><div class='cat-icon'><img src='boutique_fichiers/icon-product-park.svg'></div></div><div class='text'><h4>Futuroscope " + i + "</h4><p>Description</p><p>Ville-sur-Ville (99)</p></div></a></div>"
        body += navbar + "</ul>" if markers.length > 1
        body += "</div>"

        ibOptions.content = body
        @infobox = new InfoBox(ibOptions)
        @infobox.open(@map, markers[0])

        google.maps.event.addListener @infobox, 'domready', =>
            $('.infoBox').parent().closest('div').css('top', (if markers.length > 1 then '48px' else '23px')) 
            $(".body").tabs({ selected: 0 }) if markers.length > 1
            @map.setCenter(markers[0].position)
            @map.panBy(0,60)

    ####MARKERS####
    initialize_markers_cluster: =>
        @marker_cluster = new MarkerClusterer @map, @markers,
            zoomOnClick: false
            styles: [
                {url: "markerclusterer/m1.png", height: 78, width: 78, textColor: "#60779f", textSize: 16, backgroundPosition: '78px 78px'}
                {url: "markerclusterer/m2.png", height: 81, width: 81, textColor: "#df9a6c", textSize: 16, backgroundPosition: '81px 81px'}
                {url: "markerclusterer/m3.png", height: 92, width: 92, textColor: "#bc2c3f", textSize: 16, backgroundPosition: '92px 92px'}
                {url: "markerclusterer/m4.png", height: 103, width: 103, textColor: "#b44d9c", textSize: 16, backgroundPosition: '103px 103px'}
                {url: "markerclusterer/m5.png", height: 116, width: 116, textColor: "#75539b", textSize: 16, backgroundPosition: '116px 116px'}
            ]

        google.maps.event.addListener @marker_cluster, "clusterclick", (cluster) =>
            bounds = new google.maps.LatLngBounds()
            markers = cluster.getMarkers()
            
            for marker in markers
                is_same_location = (if prev_position? then marker.position.equals(prev_position) else true) unless is_same_location == false
                bounds.extend(marker.position)
                prev_position = marker.position

            if is_same_location
                @open_info_window(markers)
            else
                @map.fitBounds(bounds)

     set_markers: ->
        @markers = @filtered_items.map (location, i) => 
            icon = {url: @marker_icons[@filtered_items[i].t], scaledSize: new google.maps.Size(30, 30)}
            marker = new google.maps.Marker
                position: new google.maps.LatLng(@filtered_items[i].a, @filtered_items[i].o),
                map: @map,
                icon: icon,
                id: @filtered_items[i].i

            @bounds.extend(marker.position)
            marker.addListener 'click', =>
                @open_info_window([marker])
            marker

    reset_markers: (items) =>
        @items = items
        @filter_items_by_categories()
        @marker_cluster.clearMarkers()
        @set_markers()
        @marker_cluster.addMarkers(@markers)

    ####SEARCH####
    initialize_search_input: ->
        autocomplete = new google.maps.places.Autocomplete(document.getElementById("maps-search"), {
            types: ['(regions)'],
            componentRestrictions: {country: "fr"}
        })

        autocomplete.addListener 'place_changed', => 
            place = autocomplete.getPlace()
            @search(place) if place.types

    search: (place) =>
        if place
            if place.types[0] == "locality" || place.types[0] == "postal_code" then @fit_to_city(place) else @fit_to_place(place)
        else
            @get_by_geocoder($('#maps-search').val()).then (response) =>
                if response.types[0] == "locality" || response.types[0] == "postal_code" then @fit_to_city(response) else @fit_to_place(response)

    fit_to_city: (place) ->
        coords = new google.maps.LatLng({lat: place.geometry.location.lat(), lng: place.geometry.location.lng()})
        out = []

        for marker in @markers
            out.push {position: marker.position, distance: google.maps.geometry.spherical.computeDistanceBetween(marker.position, coords)}
        
        out = _.sortBy(out, 'distance')
        out = out.slice(0, 15)
        bounds = new google.maps.LatLngBounds()

        out.map (marker, i) ->
            bounds.extend(marker.position)
        
        @map.fitBounds(bounds)
        
        if @city_marker
            @city_marker.setPosition(place.geometry.location)
            @city_marker.setMap(@map)
        else
            @city_marker = new google.maps.Marker
                position: place.geometry.location,
                map: @map,
                icon: {url: "http://loisicheque.abbi-studio.fr/boutique_fichiers/pin.png", scaledSize: new google.maps.Size(30, 30)}

    fit_to_place: (place) ->
        @city_marker.setMap(null) if @city_marker
        @map.fitBounds(place.geometry.viewport)

    get_by_geocoder: (place) ->
        new Promise (resolve, reject) =>
            @geocoder.geocode {address: place, region: "fr"}, (results) =>
                resolve results[0]


$ ->
    map = null
    $.ajax
     url: "./data.json"
     type: "GET"
     success: (data) ->
        map = new LoisiMap(data.items, data.markers)

        # Fit to 15 points near the city on click radio button
        $('.radio#search').on 'click', ->
            map.search() if $('#maps-search').val()
            
        # Fit to France on click radio button
        $('.radio#country').on 'click', ->
            map.fit_to_france() if map

        # Filter by categories
        $('.category .radio').on 'click', ->
            categories = []
            for radio in $('.category .radio.checked')
                categories.push $(radio).data('id')
            
            map.set_categories(categories)

        # To reset countries on new call to server
        # map.reset_markers(items_from_server)